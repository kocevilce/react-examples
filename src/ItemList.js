
function ItemList({items, onItemClick}) {

    const handleClick = ({target}) => {
        const item = target.value;
        onItemClick(item);
    }

    return (
        <div>
                {items.map((item, index) => (
                    <button value={index} onClick={handleClick} key={index}>
                        {item}
                    </button>
                ))}
        </div>
    );
}
export default ItemList