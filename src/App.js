import './App.css';
import './GroceryCart';
import 'bootstrap/dist/css/bootstrap.min.css';
import GroceryCart from "./GroceryCart";
import EditProfile from "./EditProfileForm";
import AppTasks from "./TasksPage/AppTasks";

function App() {
  return (
    <div className="App">
        <AppTasks />
    </div>
  );
}

export default App;
