import React, {useState} from "react";
import Form from 'react-bootstrap/Form';
import { Button, Container } from 'react-bootstrap';


function NewTask({handleSubmit}) {

    const [task, setTask] = useState({});

    const onSubmitNewTask = (event) => {
        event.preventDefault();
        if (!task.title) return;
        task.id = Math.floor(Math.random() * 100_000_000);
        handleSubmit(task);
        setTask({});
    }

    const handleChange = ({target}) => {
        const {name, value} = target;
        setTask((prev) => ({...prev, id:Math.floor(Math.random() * 100000000), [name]: value}))
    }

    return(

        <Form onSubmit={onSubmitNewTask}>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                <Form.Control
                    value={task.title || ''}
                    name="title"
                    type="text"
                    onChange={handleChange}
                    placeholder="Task title"
                />
            </Form.Group>


            <Button type="submit" >Submit</Button>
        </Form>

    )
}
export default NewTask