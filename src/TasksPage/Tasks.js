import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";

function Tasks(props) {

    const handleClick = (taskId) => {
        props.handleDelete(taskId)
    }

    return(
        <Table striped bordered hover>
            <thead>
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Delete </th>
            </tr>
            </thead>
            <tbody>
            {props.tasks.map((task, index) => (
                <tr key={index}>
                    <td > {index + 1} </td>
                    <td > {task.title} </td>
                    <td> <Button variant="danger" onClick={() => handleClick(task.id)} >  remove  </Button> </td>
                </tr>
            ))}
            </tbody>

        </Table>
    )

}
export default Tasks