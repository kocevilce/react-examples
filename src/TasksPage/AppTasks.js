
import React, { useState, useEffect } from 'react';
import NewTask from "./NewTask";
import Tasks from "./Tasks";
import {Container} from "react-bootstrap";
import tasks from "./Tasks";
import {deleteTask, getTasks, setTask} from "../services/task";
import {isAccordionItemSelected} from "react-bootstrap/AccordionContext";

function AppTasks() {

  const [tasks, setTasks] = useState([])
    useEffect(() => {
        getTasks()
            .then(items => {
                setTasks(items)
            })
    }, [])

  const handleSubmit = (task) => {
      // setTasks((prev) => {
      //     return [task, ...prev];
      // })
      setTask(task).then(items  => {
          setTasks(items)
      })
  }

  const handleDelete = (taskIdToRemove) => {
      deleteTask(taskIdToRemove).then(items => {
          setTasks(items)
      })
      // setTasks((prev) => {
      //     return prev.filter((task, i) => task.id !== taskIdToRemove);
      // });

  }

  return (
      <div>
          <Container>
          <h1> Tasks </h1>
          <NewTask
              handleSubmit={handleSubmit}
          />

          <br />
          <Tasks tasks={tasks}  handleDelete={handleDelete}/>

        </ Container>

      </div>
  )


}
export default AppTasks