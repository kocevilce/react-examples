export function getTasks() {
    return fetch('http://localhost:4000/tasks')
        .then(data => data.json())
}

export function setTask(task) {
    return fetch('http://localhost:4000/task', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ task })
    })
        .then(data => data.json())
}

export function deleteTask(taskId) {
    return fetch(`http://localhost:4000/task/${taskId}`,{method: 'DELETE'} )
        .then(data => data.json())
}